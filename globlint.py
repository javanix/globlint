#!/usr/bin/env python

import mmap
import os
import re
import sys
from enum import IntEnum, auto
from io import StringIO, TextIOWrapper
from itertools import chain
from pathlib import Path
from stat import S_ISDIR
from typing import IO, Any, Dict, Iterable, List, Optional, Set, Sized, Tuple

import click
import requests
import yaml
from anytree import Node, PreOrderIter  # type: ignore
from pydantic import BaseModel, ByteSize, Field  # type: ignore
from tabulate import tabulate
from wcmatch import glob  # type: ignore

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" #

ENVVAR = "GLOBLINT"
ENVVAR_TARGET = f"{ENVVAR}_TARGET"
ENVVAR_OUTPUT = f"{ENVVAR}_OUTPUT"
ENVVAR_CONFIG = f"{ENVVAR}_CONFIG"

DEFAULT_REPORT_INDENT = 2
DEFAULT_OUTPUT = "globlint.json"
DEFAULT_CONFIG = ".globlintrc.yml"

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" #

URL_REGEX = re.compile(
    r"^"
    # Protocol identifier
    r"(?:(?:https?|ftp)://)"
    # user:password authentication
    r"(?:\S+(?::\S*)?@)?" r"(?:"
    # IP address exclusion
    # private & local networks
    r"(?!(?:10|127)(?:\.\d{1,3}){3})"
    r"(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})"
    r"(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})"
    # IP address dotted notation octets
    # excludes loopback network 0.0.0.0
    # excludes reserved space >= 224.0.0.0
    # excludes network & broadcast addresses
    # (first & last IP address of each class)
    r"(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])"
    r"(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}"
    r"(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))"
    r"|"
    r"(?:"
    r"(?:localhost|invalid|test|example)|("
    # Host name
    r"(?:(?:[A-z\u00a1-\uffff0-9]-*_*)*[A-z\u00a1-\uffff0-9]+)"
    # Domain name
    r"(?:\.(?:[A-z\u00a1-\uffff0-9]-*)*[A-z\u00a1-\uffff0-9]+)*"
    # TLD identifier
    r"(?:\.(?:[A-z\u00a1-\uffff]{2,}))" r")))"
    # Port number
    r"(?::\d{2,5})?"
    # Resource path
    r"(?:/\S*)?" r"$",
    re.U,
)


class UrlOrFile(click.File):
    def __init__(self):
        super().__init__("r", encoding="utf-8")

    def convert(self, value, param, ctx) -> IO[Any]:
        if URL_REGEX.match(value):
            try:
                # Disable SSL verification to allow self-signed certificates
                response = requests.get(value, verify=False)  # nosec
                if response.status_code != 200:
                    raise click.FileError(filename=value)
            except click.FileError as error:
                raise error
            except Exception:
                raise click.FileError(filename=value)

            return StringIO(response.text)

        return super().convert(value, param, ctx)


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" #

TARGET = click.Path(exists=True, file_okay=False, resolve_path=True)
CONFIG = UrlOrFile()
OUTPUT = click.File("w", encoding="utf-8", lazy=True)

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" #


class QuantityConstraint(BaseModel):
    min: int = Field(default=1, ge=1)
    max: Optional[int]
    detail: Optional[str]


class SizeConstraint(BaseModel):
    min: ByteSize = 0  # type: ignore
    max: Optional[ByteSize]
    detail: Optional[str]


class ContentConstraint(BaseModel):
    if_: str = Field(alias="if")
    allow: bool = True
    detail: Optional[str]


class Constraints(BaseModel):
    ignore: bool = False
    optional: bool = False
    quantity: Optional[QuantityConstraint]
    size: Optional[SizeConstraint]
    content: List[ContentConstraint] = []
    detail: Optional[str]


class Rule(BaseModel):
    pattern: Optional[str] = Field(alias="__pattern__")
    constraints: Optional[Constraints] = Field(alias="__constraints__")


class Unknown(BaseModel):
    detail: Optional[str]


class UnknownsConfig(BaseModel):
    dir: Optional[Unknown]
    file: Optional[Unknown]


class Config(BaseModel):
    target: Dict[str, Any]
    unknowns: Optional[UnknownsConfig]


class Message(BaseModel):
    path: Path
    detail: str


class Report(BaseModel):
    target: Path
    warnings: List[Message]
    errors: List[Message]

    @property
    def status(self) -> int:
        return 1 if self.errors else 0


class CheckError(Exception):
    __slots__ = ("detail",)

    def __init__(self, detail: str):
        super().__init__(detail)
        self.detail = detail


class CheckStatus(IntEnum):
    OKAY = auto()
    IGNORE = auto()
    OPTIONAL = auto()
    ERROR = auto()


class Check(BaseModel):
    status: CheckStatus
    pattern: str
    path: Path
    detail: Optional[str]

    @classmethod
    def okay(cls, pattern: str, path: Path) -> "Check":
        return cls(status=CheckStatus.OKAY, pattern=pattern, path=path)

    @classmethod
    def ignore(cls, pattern: str, path: Path) -> "Check":
        return cls(status=CheckStatus.IGNORE, pattern=pattern, path=path)

    @classmethod
    def optional(cls, pattern: str, path: Path, detail: str) -> "Check":
        return cls(
            status=CheckStatus.OPTIONAL,
            pattern=pattern,
            path=path,
            detail=detail,
        )

    @classmethod
    def error(cls, pattern: str, path: Path, detail: str) -> "Check":
        return cls(
            status=CheckStatus.ERROR,
            pattern=pattern,
            path=path,
            detail=detail,
        )


Check.update_forward_refs()


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" #


def is_dir(path: Path) -> bool:
    return S_ISDIR(path.stat().st_mode)


def relative_to(path: Path, other: Path) -> Path:
    try:
        return path.relative_to(other)
    except ValueError:
        return path


def load_config(stream: TextIOWrapper) -> Config:
    return Config.parse_obj(yaml.full_load(stream))


def extract_rules(
    parent: Node, target: Dict[str, Any], exclude: Set[str]
) -> Iterable[Node]:
    items = filter(lambda x: x[0] not in exclude, target.items())
    for name, item in items:
        rule = Rule.parse_obj(item) if item else Rule()
        if rule.pattern is None:
            rule.pattern = name

        node = Node(name, parent, rule=rule)
        yield node
        if item:
            yield from extract_rules(node, item, exclude)


def create_rules(config: Config, name: str = "__target__") -> Node:
    meta = set(map(lambda x: x.alias, Rule.__fields__.values()))
    root = Node(name)

    for _ in extract_rules(root, config.target, exclude=meta):
        pass

    return root


def check_quantity(
    target: Path, rule: Rule, path: Path, paths: Sized
) -> Optional[CheckError]:
    if not rule.constraints or not rule.constraints.quantity:
        return None

    constraint = rule.constraints.quantity
    actual = len(paths)

    if constraint.min > actual:
        detail = f"Quantity is lower than {constraint.min}"
    elif constraint.max is not None and constraint.max < actual:
        detail = f"Quantity is greater than {constraint.max}"
    else:
        return None

    if constraint.detail:
        return CheckError(
            constraint.detail.format(
                target=target,
                pattern=rule.pattern,
                path=relative_to(path, target),
                actual=actual,
                min=constraint.min,
                max=constraint.max,
            )
        )

    if constraint.max is None:
        expected_range = f"[{constraint.min}, ...]"
    else:
        expected_range = f"[{constraint.min}, {constraint.max}]"

    return CheckError(
        f"{detail}, acceptable quantity is in range {expected_range}"
    )


def check_size(target: Path, rule: Rule, path: Path) -> List[CheckError]:
    if not rule.constraints or not rule.constraints.size:
        return []

    constraint = rule.constraints.size
    actual = path.stat().st_size

    if constraint.min > actual:
        detail = f"Size is lower than {constraint.min}"
    elif constraint.max is not None and constraint.max < actual:
        detail = f"Size is greater than {constraint.max}"
    else:
        return []

    if constraint.detail:
        return [
            CheckError(
                constraint.detail.format(
                    target=target,
                    pattern=rule.pattern,
                    path=relative_to(path, target),
                    actual=actual,
                    min=constraint.min,
                    max=constraint.max,
                )
            )
        ]

    if constraint.max is None:
        expected_range = f"[{constraint.min}, ...]"
    else:
        expected_range = f"[{constraint.min}, {constraint.max}]"

    return [
        CheckError(
            f"{detail} byte(s), acceptable size is in "
            f"range {expected_range} byte(s)"
        )
    ]


def check_required_content(
    target: Path,
    rule: Rule,
    path: Path,
    content: mmap.mmap,
    constraints: Iterable[ContentConstraint],
) -> Iterable[CheckError]:
    for constraint in constraints:
        regex = re.compile(constraint.if_.encode("utf-8"), re.M)
        if not regex.search(content):  # type: ignore
            if constraint.detail:
                detail = constraint.detail.format(
                    target=target,
                    pattern=rule.pattern,
                    path=relative_to(path, target),
                    **{"if": constraint.if_},
                )
            else:
                detail = f"Content r'{constraint.if_}' is required"

            yield CheckError(detail=detail)


def check_forbidden_content(
    target: Path,
    rule: Rule,
    path: Path,
    content: mmap.mmap,
    constraints: Iterable[ContentConstraint],
) -> Iterable[CheckError]:
    for constraint in constraints:
        regex = re.compile(constraint.if_.encode("utf-8"), re.M)
        if regex.search(content):  # type: ignore
            if constraint.detail:
                detail = constraint.detail.format(
                    target=target,
                    pattern=rule.pattern,
                    path=relative_to(path, target),
                    **{"if": constraint.if_},
                )
            else:
                detail = f"Content r'{constraint.if_}' is forbidden"

            yield CheckError(detail=detail)


def check_content(target: Path, rule: Rule, path: Path) -> List[CheckError]:
    if (
        not rule.constraints
        or not rule.constraints.content
        or is_dir(path)
        or path.stat().st_size == 0
    ):
        return []

    errors: List[CheckError] = []

    required = filter(lambda x: x.allow, rule.constraints.content)
    forbidden = filter(lambda x: not x.allow, rule.constraints.content)

    with path.open("rb") as file:
        content = mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
        errors.extend(
            check_required_content(target, rule, path, content, required)
        )
        errors.extend(
            check_forbidden_content(target, rule, path, content, forbidden)
        )

    return errors


def join_patterns(rule: Rule, parents: List[Rule]) -> str:
    return "/".join(
        [
            *map(lambda x: x.pattern, parents),  # type: ignore
            rule.pattern,  # type: ignore
        ]
    )


def check_paths(
    target: Path, rule: Rule, pattern: str
) -> Tuple[Set[Path], Dict[Path, List[CheckError]]]:
    successful, unsuccessful = set(), {}

    for item in glob.iglob(
        pattern, root_dir=target, limit=0, flags=glob.G | glob.B | glob.D
    ):
        path = target.joinpath(item)

        errors = list(
            chain(
                check_size(target, rule, path),
                check_content(target, rule, path),
            )
        )
        if errors:
            unsuccessful[item] = errors

        successful.add(item)

    return successful, unsuccessful


def checks_from_unsuccessful(
    pattern: str, unsuccessful: Dict[Path, List[CheckError]]
) -> List[Check]:
    checks = []
    for item, errors in unsuccessful.items():
        for error in errors:
            checks.append(Check.error(pattern, item, error.detail))

    return checks


def not_exists_check(
    target: Path, rule: Rule, path: Path, pattern: str
) -> Check:
    if rule.constraints and rule.constraints.detail:
        detail = rule.constraints.detail.format(
            target=target,
            pattern=rule.pattern,
            path=relative_to(path, target),
        )
    else:
        detail = "Not exists"

    if rule.constraints and rule.constraints.optional:
        return Check.optional(pattern, path, detail)
    else:
        return Check.error(pattern, path, detail)


def check_rule(target: Path, node: Node, parents: List[Rule]) -> List[Check]:
    rule: Rule = node.rule  # noqa

    pattern = join_patterns(rule, parents)
    path = target.joinpath(pattern)

    if rule.constraints and rule.constraints.ignore:
        return [Check.ignore(pattern, path)]

    successful, unsuccessful = check_paths(target, rule, pattern)
    paths = successful | set(unsuccessful.keys())

    if rule.constraints and rule.constraints.quantity:
        check = check_quantity(target, rule, path, paths)
        if check:
            unsuccessful[path] = [check]

    if unsuccessful:
        return checks_from_unsuccessful(pattern, unsuccessful)

    if not successful:
        return [not_exists_check(target, rule, path, pattern)]

    return [Check.okay(pattern, path)]


def toplevel_nodes(tree: Node) -> Iterable[Node]:
    return PreOrderIter(tree, maxlevel=2, filter_=lambda x: x.name != tree.name)


def check_rules(
    target: Path, rules: Node, parents: Optional[List[Rule]] = None
) -> Iterable[Check]:
    if parents is None:
        parents = []

    for node in toplevel_nodes(rules):
        rule: Rule = node.rule  # noqa

        checks = check_rule(target, node, parents)
        for check in checks:
            yield check

        check = checks[0]
        if (
            check.status not in {CheckStatus.OPTIONAL, CheckStatus.ERROR}
            and not node.is_leaf
        ):
            yield from check_rules(target, node, parents + [rule])


def filter_redundant(
    paths: Iterable[Path], lookup: Optional[Set[Path]] = None
) -> Iterable[Path]:
    lookup = set() if lookup is None else lookup
    for path in sorted(paths):
        is_redundant = False
        for item in lookup:
            is_redundant = str(path).startswith(f"{str(item)}{os.sep}")
            if is_redundant:
                break

        if not is_redundant:
            lookup.add(path)
            yield path


def find_known(target: Path, checks: Iterable[Check]) -> Set[str]:
    known = set()
    for check in checks:
        known.update(
            glob.iglob(
                check.pattern,
                root_dir=target,
                limit=0,
                flags=glob.G | glob.B | glob.D,
            )
        )

        if check.status is CheckStatus.IGNORE:
            known.update(
                glob.iglob(
                    f"{check.pattern}/**/*",
                    root_dir=target,
                    limit=0,
                    flags=glob.G | glob.D,
                )
            )

    return known


def find_unknown(target: Path, known: Set[str]) -> Tuple[Set[Path], Set[Path]]:
    unknown = set()
    for path in glob.iglob(
        "**/*", root_dir=target, limit=0, flags=glob.G | glob.D
    ):
        if path not in known:
            unknown.add(target.joinpath(path))

    unknown_dirs = set(filter(lambda x: is_dir(x), unknown))
    unknown_files = unknown - unknown_dirs

    return unknown_dirs, unknown_files


def check_target(
    target: Path, config: Optional[UnknownsConfig], checks: Iterable[Check]
) -> Iterable[Message]:
    known = find_known(target, checks)
    unknown_dirs, unknown_files = find_unknown(target, known)

    lookup: Set[Path] = set()

    for path in filter_redundant(unknown_dirs, lookup):
        if config and config.dir and config.dir.detail:
            detail = config.dir.detail.format(
                target=target, path=relative_to(path, target)
            )
        else:
            detail = "Unknown directory"

        yield Message(path=path, detail=detail)

    for path in filter_redundant(unknown_files, lookup):
        if config and config.file and config.file.detail:
            detail = config.file.detail.format(
                target=target, path=relative_to(path, target)
            )
        else:
            detail = "Unknown file"

        yield Message(path=path, detail=detail)


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" #


def extract_errors(checks: Iterable[Check]) -> Iterable[Message]:
    return map(
        lambda x: Message(**x.dict(include={"path", "detail"})),
        filter(lambda x: x.status is CheckStatus.ERROR, checks),
    )


def extract_warnings(checks: Iterable[Check]) -> Iterable[Message]:
    return map(
        lambda x: Message(**x.dict(include={"path", "detail"})),
        filter(lambda x: x.status is CheckStatus.OPTIONAL, checks),
    )


def prepare_errors(
    target: Path, config: Config, checks: Iterable[Check]
) -> Iterable[Message]:
    for error in chain(
        check_target(target, config.unknowns, checks), extract_errors(checks)
    ):
        error.path = relative_to(error.path, target)
        yield error


def prepare_warnings(
    target: Path, checks: Iterable[Check]
) -> Iterable[Message]:
    for warning in extract_warnings(checks):
        warning.path = relative_to(warning.path, target)
        yield warning


def create_report(target: Path, config: Config, rules: Node) -> Report:
    checks = list(check_rules(target, rules))
    warnings = sorted(prepare_warnings(target, checks), key=lambda x: x.path)
    errors = sorted(
        prepare_errors(target, config, checks), key=lambda x: x.path
    )
    return Report(target=target, warnings=warnings, errors=errors)


def format_messages(messages: Iterable[Tuple[str, Message]]) -> str:
    return tabulate(
        map(lambda x: [x[0], x[1].path, x[1].detail], messages),
        headers=["Type", "Path", "Detail"],
        showindex="always",
    )


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" #


@click.command()
@click.option("--pretty", is_flag=True, help="Indentations in a report.")
@click.option("--verbose", is_flag=True, help="Show warnings and errors.")
@click.option("--dry-run", is_flag=True, help="Validate config and exit.")
@click.argument("target", type=TARGET, envvar=ENVVAR_TARGET, required=False)
@click.argument(
    "config", type=CONFIG, envvar=ENVVAR_CONFIG, default=DEFAULT_CONFIG
)
@click.argument(
    "output", type=OUTPUT, envvar=ENVVAR_OUTPUT, default=DEFAULT_OUTPUT
)
def main(
    target: str,
    output: TextIOWrapper,
    config: TextIOWrapper,
    pretty: bool,
    verbose: bool,
    dry_run: bool,
):
    """A tool for detecting inconsistencies in TARGET structure. All rules should
    be described in CONFIG. The CONFIG is a YAML file (default: .globlintrc.yml).
    A report after checking the TARGET is available in OUTPUT. The OUTPUT is a JSON
    file (default: globlint.json) with listed warnings and errors in the TARGET.

    \b
    Example CONFIG:
    \b
    ```yml
    target:
      .git:
        __constraints__:
          ignore: True # default: False
          detail: # custom message: {target}, {path}, {pattern}

      foo:
        bar.txt:
          __constraints__:
            optional: True # default: False

            quantity:
              min: 2 # default: 1
              max: 10 # default: None
              detail: # custom message:
                      # {target}, {path}, {pattern}, {min}, {max}, {actual}

            content: # default: []
              - if: "foo" # any regular expression
              - if: "bar"
                allow: False # default: True
                detail: # custom message: {target}, {path}, {pattern}, {if}

            size:
              min: 1 MB # default: 0
              max: 10 MB # default: None
              detail: # custom message:
                      # {target}, {path}, {pattern}, {min}, {max}, {actual}

    unknowns:
      file:
        detail: # custom message: {target}, {path}

      dir:
        detail: # custom message: {target}, {path}
    ```
    """

    loaded_config = load_config(config)
    if dry_run:
        click.secho("All good!", fg="green")
        sys.exit(0)

    if not target:
        click.secho("No target!", fg="red")
        sys.exit(1)

    report = create_report(
        Path(target), loaded_config, create_rules(loaded_config)
    )
    output.write(report.json(indent=DEFAULT_REPORT_INDENT if pretty else None))

    if verbose:
        click.echo(f"Target: {report.target}")
        if report.errors or report.warnings:
            click.echo(
                format_messages(
                    chain(
                        map(lambda x: ("Warning", x), report.warnings),
                        map(lambda x: ("Error", x), report.errors),
                    )
                )
            )

    if not report.errors and not report.warnings:
        click.secho("All good!", fg="green")

    if report.warnings:
        click.secho(f"{len(report.warnings)} Warning(s)!", fg="yellow")

    if report.errors:
        click.secho(f"{len(report.errors)} Error(s)!", fg="red")

    sys.exit(report.status)


if __name__ == "__main__":
    main()
